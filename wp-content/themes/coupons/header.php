<!doctype html>
<html lang="en">

<head>
	<?php wp_head(); ?>
    <!-- Required meta tags -->
    <style>
        .items-loop .item .bottom a .coupon:after {
             background: url(<?php echo get_template_directory_uri(); ?>/images/button.png) no-repeat right top;
        }
    </style>
</head>
<body <?php body_class(); ?>>
<div class="main-header gradient">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="logo">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="Couponramy.com">
                        <h2><?php bloginfo('namw'); ?></h2>
                    </a>
                    <p>
                        <?php bloginfo('description'); ?>
                    </p>
                </div>
                <div class="mobile-button">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-right"></i>
                    </button>
                </div>
            </div>
            <div class="col-md-8">
                <nav class="navbar navbar-expand-lg float-right">
					<?php
					wp_nav_menu( array(
						'theme_location'  => 'header_menu',
						'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
						'container'       => 'div',
						'container_class' => 'collapse navbar-collapse',
						'container_id'    => 'navbarSupportedContent',
						'menu_class'      => 'navbar-nav',
						'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
						'walker'          => new WP_Bootstrap_Navwalker(),
					) );
					?>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- end of main header-->