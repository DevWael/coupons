<?php if ( ! defined( 'ABSPATH' ) ) {
	die;
} // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// TAXONOMY OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options = array();

// -----------------------------------------
// Taxonomy Options                        -
// -----------------------------------------
$options[] = array(
	'id'       => 'brands_taxonomy_options',
	'taxonomy' => 'brands', // category, post_tag or your custom taxonomy name
	'fields'   => array(


		array(
			'id'    => 'brand_logo',
			'type'  => 'image',
			'title' => 'Brand Logo',
			'add_title' => 'Add Logo'
		),
		array(
			'id'    => 'affiliate_url',
			'type'  => 'text',
			'title' => 'Affiliate URL',
		),

	),
);

CSFramework_Taxonomy::instance( $options );
