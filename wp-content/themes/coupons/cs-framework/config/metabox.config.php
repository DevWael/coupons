<?php if ( ! defined( 'ABSPATH' ) ) {
	die;
} // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// METABOX OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options = array();

// -----------------------------------------
// Page Metabox Options                    -
// -----------------------------------------
$options[] = array(
	'id'        => 'coupons_settings',
	'title'     => 'Add Coupon Code',
	'post_type' => 'coupons',
	'context'   => 'normal',
	'priority'  => 'default',
	'sections'  => array(

		// begin: a section
		array(
			'name'   => 'section_1',
			'icon'   => 'fa fa-cog',
			// begin: fields
			'fields' => array(
				array(
					'id'         => 'coupon_code',
					'type'       => 'text',
					'title'      => 'Coupon Code',
					'attributes' => array(
						'style' => 'width: 325px;'
					)
				),
				array(
					'id'         => 'deal_url',
					'type'       => 'text',
					'title'      => 'Get Deal URL',
					'attributes' => array(
						'style' => 'width: 325px;'
					)
				),
				array(
					'id'         => 'discount',
					'type'       => 'text',
					'title'      => 'Discount Percent',
					'after'      => ' %',
					'attributes' => array(
						'style'       => 'width: 325px;',
						'placeholder' => '10',
					),
					'validate'   => 'numeric'
				),
				array(
					'id'         => 'expiry_date',
					'type'       => 'text',
					'title'      => 'Expiry Date',
					'attributes' => array(
						'style'       => 'width: 325px;',
						'placeholder' => '31-6-2018',
					),
				),
				// end: a field
			) // end: fields
		) // end: a section
	)
);

//CSFramework_Metabox::instance( $options );
