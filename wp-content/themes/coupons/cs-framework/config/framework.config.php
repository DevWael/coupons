<?php if ( ! defined( 'ABSPATH' ) ) {
	die;
} // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$settings = array(
	'menu_title'      => 'Site settings',
	'menu_type'       => 'menu', // menu, submenu, options, theme, etc.
	'menu_slug'       => 'cs-framework',
	'ajax_save'       => true,
	'show_reset_all'  => true,
	'framework_title' => 'Devo Framework <small><a target="_blank" href="https://devolum.com">by devolum</a></small>',
);

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options = array();

// ----------------------------------------
// a option section for options overview  -
// ----------------------------------------
$options[] = array(
	'name'   => 'overwiew',
	'title'  => 'Overview',
	'icon'   => 'fa fa-star',
	// begin: fields
	'fields' => array(
		array(
			'id'    => 'hero_title',
			'type'  => 'text',
			'title' => 'Hero Section Title',
		),
		array(
			'id'    => 'hero_desc',
			'type'  => 'textarea',
			'title' => 'Hero Section Description',
		),


		// begin: a field
		array(
			'id'    => 'facebook',
			'type'  => 'text',
			'title' => 'facebook',
		),
		array(
			'id'    => 'twitter',
			'type'  => 'text',
			'title' => 'twitter',
		),
		array(
			'id'    => 'instagram',
			'type'  => 'text',
			'title' => 'instagram',
		),
		// end: a field

	) // end: fields
);

// ------------------------------
// backup                       -
// ------------------------------
$options[] = array(
	'name'   => 'backup_section',
	'title'  => 'Backup',
	'icon'   => 'fa fa-shield',
	'fields' => array(

		array(
			'type'    => 'notice',
			'class'   => 'warning',
			'content' => 'You can save your current options. Download a Backup and Import.',
		),

		array(
			'type' => 'backup',
		),

	)
);

CSFramework::instance( $settings, $options );
