<?php
require_once COUPONS_CORE . 'menu-walker.php'; //Bootstrap 4 nav walker
require_once get_template_directory() . '/cs-framework/cs-framework.php';
define( 'CS_ACTIVE_SHORTCODE', false ); // default true
define( 'CS_ACTIVE_CUSTOMIZE', false ); // default true
define( 'CS_ACTIVE_LIGHT_THEME', true ); // default false

function coupons_get_setting( $option_id, $default = '' ) {
	return cs_get_option( $option_id, $default );
}

add_action( 'after_setup_theme', 'coupons_theme_setup' );
if ( ! function_exists( 'coupons_theme_setup' ) ) {
	function coupons_theme_setup() {
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'coupons_slider_double', 600, 450, true );
		add_image_size( 'image_100_100', 100, 100, true );
		add_image_size( 'image_260_160', 260, 160, true );
		add_image_size( 'image_350_220', 350, 220, true );
		load_theme_textdomain( 'window-mag', get_template_directory() . '/languages' );
		register_nav_menus( array(
			'header_menu' => esc_html__( 'Primary Menu', 'window-mag' ),
		) );
	}
}

add_action( 'wp_enqueue_scripts', 'coupons_enqueue_scripts' );
if ( ! function_exists( 'coupons_enqueue_scripts' ) ) {
	function coupons_enqueue_scripts() {
		wp_enqueue_style( 'boostrap-4',  get_template_directory_uri() . '/css/bootstrap.min.css' );
		wp_enqueue_style( 'f-awesome',  get_template_directory_uri() . '/css/awesome/all.css' );
		wp_enqueue_style( 'style', get_stylesheet_uri() );
		wp_enqueue_style( 'custom-style', get_template_directory_uri() . '/css/custom-style.css' );
		wp_enqueue_style( 'style-responsive', get_template_directory_uri() . '/responsive.css' );

		wp_enqueue_script('jquery3', get_template_directory_uri() . '/js/jquery.min.js', array(), null, true);
		wp_enqueue_script( 'popperjs', get_template_directory_uri() . '/js/popper.min.js',  array(), '1.0.0', true   );
		wp_enqueue_script( 'bootstrapjs4', get_template_directory_uri() . '/js/bootstrap.min.js',  array(), '1.0.0', true  );
		wp_enqueue_script( 'main-js', get_template_directory_uri() . '/js/main.js',   array(), '1.0.0', true  );

		if ( is_singular() ) {
			//wp_enqueue_script( "comment-reply" );
		}
	}
}

add_filter( 'update_footer', 'coupons_footer_version', 12 );
function coupons_footer_version( $html ) {
	if ( ( current_user_can( 'update_themes' ) || current_user_can( 'update_plugins' ) ) ) {
		return ( empty( $html ) ? '' : $html . ' | ' ) . wp_get_theme()->name . ' ' .
		       wp_get_theme()->version . ' | By <a href="https://devolum.com" target="_blank">Devolum Team</a>';
	} else {
		return $html;
	}
}

if ( ! function_exists( 'coupons_pagination' ) ) {
	function coupons_pagination() {
		if ( is_singular() ) {
			return;
		}
		global $wp_query;
		if ( $wp_query->max_num_pages <= 1 ) {
			return;
		}
		$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
		$max   = intval( $wp_query->max_num_pages );
		if ( $paged >= 1 ) {
			$links[] = $paged;
		}
		if ( $paged >= 3 ) {
			$links[] = $paged - 1;
			$links[] = $paged - 2;
		}
		if ( ( $paged + 2 ) <= $max ) {
			$links[] = $paged + 2;
			$links[] = $paged + 1;
		}
		echo '<div class="pagePagination"><ul>' . "\n";
		if ( ! in_array( 1, $links ) ) {
			$class = 1 == $paged ? ' class="active"' : '';
			printf( '<li%s><a href="%s">%s</a href="%s"></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1','');
			if ( ! in_array( 2, $links ) ) {
				echo '<li><span>...</span></li>';
			}
		}
		sort( $links );
		foreach ( (array) $links as $link ) {
			$class = $paged == $link ? ' class="active"' : '';
			printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
		}
		if ( ! in_array( $max, $links ) ) {
			if ( ! in_array( $max - 1, $links ) ) {
				echo '<li><span>...</span></li>' . "\n";
			}

			$class = $paged == $max ? ' class="active"' : '';
			printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
		}
		echo '</ul></div>' . "\n";
	}
}

add_action( 'wp_head', 'coupons_open_graph' );
if ( ! function_exists( 'coupons_open_graph' ) ) {
	function coupons_open_graph() {
		global $post;
		$post_thumb = $image_url_brand='';
		if ( function_exists( "has_post_thumbnail" ) && has_post_thumbnail() ) {
			$post_thumb = get_the_post_thumbnail_url( $post->ID );
		}
		$title       = get_bloginfo( 'name' );
		$description = get_bloginfo( 'description' );
		$type        = 'website';
		if ( is_singular() ) {
			$title       = strip_shortcodes( strip_tags( ( get_the_title() ) ) ) . ' - ' . get_bloginfo( 'name' );
			$description = strip_tags( $post->post_excerpt );
			$type        = 'article';
			?>
            <meta property="og:url" content="<?php the_permalink(); ?>"/>
			<?php
          $terms            = wp_get_post_terms( get_the_ID(), 'brands' );
          if($terms){
          $image_id = get_term_meta( $terms[0]->term_id, 'brands_taxonomy_options', true )['brand_logo'];
          $image_url_brand = wp_get_attachment_image_src( $image_id, 'full' )[0];
          }
          if($image_url_brand){
          $post_thumb = $image_url_brand;
          }
		}
		?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta property="og:title" content="<?php echo esc_attr( $title ); ?>"/>
        <meta property="og:type" content="<?php echo esc_attr( $type ); ?>"/>
        <meta property="og:description" content="<?php echo esc_attr( wp_html_excerpt( $description, 100 ) ); ?>"/>
        <meta property="og:site_name" content="<?php echo esc_attr( get_bloginfo( 'name' ) ) ?>"/>
		<?php
		if ( ! empty( $post_thumb ) && is_singular() ) {
			echo '<meta property="og:image" content="' . esc_url( $post_thumb ) . '" />' . "\n";
          ?>
        <meta property="og:image:width" content="520" />
        <meta property="og:image:height" content="320" />
<?php
		}
	}
}

/**
 * This function returns the views count for the current post id
 *
 * @param $postID -> The current post id
 *
 * @return string -> post views count
 */
function coupons_getPostViews( $postID = '' ) {
	if ( ! $postID ) {
		$postID = get_the_ID();
	}
	$count_key = 'devolum_post_views';
	$count     = get_post_meta( $postID, $count_key, true );
	if ( $count == '' ) {
		delete_post_meta( $postID, $count_key );
		add_post_meta( $postID, $count_key, '0' );

		return esc_html__( '0', 'window-mag' );
	} else {
		return $count;
	}
}


function coupons_setPostViews( $postID ) {
	$count_key = 'devolum_post_views';
	$count     = get_post_meta( $postID, $count_key, true );
	if ( $count == '' ) {
		$count = 0;
		delete_post_meta( $postID, $count_key );
		add_post_meta( $postID, $count_key, '0' );
	} else {
		$count ++;
		update_post_meta( $postID, $count_key, $count );
	}
}

/**
 * This function set post view count for the current post id only for visitor not the logged-in user
 *
 * @param $post_id -> The current post id
 */
add_action( 'wp_head', 'coupons_count_popular_posts' );
function coupons_count_popular_posts( $post_id ) {
	if ( ! is_singular() ) {
		return;
	}
	if ( ! is_user_logged_in() ) {
		if ( empty ( $post_id ) ) {
			global $post;
			$post_id = $post->ID;
		}
		coupons_setPostViews( $post_id );
	}
}

// Add it to a column in WP-Admin
add_filter( 'manage_posts_columns', 'coupons_posts_column_views' );
function coupons_posts_column_views( $defaults ) {
	$defaults['post_views'] = esc_html__( 'Views Count', 'window-mag' );

	return $defaults;
}

add_action( 'manage_posts_custom_column', 'coupons_posts_custom_column_views', 5, 2 );
function coupons_posts_custom_column_views( $column_name ) {
	if ( $column_name === 'post_views' ) {
		echo (int) get_post_meta( get_the_ID(), 'devolum_post_views', true );
	}
}

if ( ! function_exists( 'coupons_nav_fallback' ) ) {
	function coupons_nav_fallback() {
		echo '<ul class="navbar-alert"><li><span class="fall-back">' . esc_html__( 'Use WP menu builder to build menus', 'window-mag' ) . '</span></li></ul>';
	}
}

add_filter( 'pre_get_posts', 'coupons_search_filter' );
function coupons_search_filter( $query ) {
	if ( is_search() && $query->is_main_query() ) {
		if ( 'yes' === coupons_get_setting( 'search_exclude' ) && ! is_admin() ) {
			$post_types = get_post_types( array( 'public' => true, 'exclude_from_search' => false ) );
			unset( $post_types['page'] );
			$query->set( 'post_type', $post_types );
		}
	}

	return $query;
}

add_action( 'init', 'coupons_post_type', 0 );
function coupons_post_type() {
	// Set UI labels for Custom Post Type
	$labels = array(
		'name'               => esc_html_x( 'Coupons', 'Post Type General Name', 'coupons' ),
		'singular_name'      => esc_html_x( 'Coupon', 'Post Type Singular Name', 'coupons' ),
		'menu_name'          => esc_html__( 'Coupons', 'coupons' ),
		'parent_item_colon'  => esc_html__( 'Parent Coupon', 'coupons' ),
		'all_items'          => esc_html__( 'All Coupons', 'coupons' ),
		'view_item'          => esc_html__( 'View Coupon', 'coupons' ),
		'add_new_item'       => esc_html__( 'Add New Coupon', 'coupons' ),
		'add_new'            => esc_html__( 'Add New', 'coupons' ),
		'edit_item'          => esc_html__( 'Edit Coupon', 'coupons' ),
		'update_item'        => esc_html__( 'Update Coupon', 'coupons' ),
		'search_items'       => esc_html__( 'Search Coupon', 'coupons' ),
		'not_found'          => esc_html__( 'Not Found', 'coupons' ),
		'not_found_in_trash' => esc_html__( 'Not found in Trash', 'coupons' ),
	);

	// Set other options for Custom Post Type

	$args = array(
		'label'               => esc_html__( 'Coupons', 'coupons' ),
		'description'         => esc_html__( 'Coupons Codes', 'coupons' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array(
			'title',
			'thumbnail',
			'revisions',
            'excerpt'
		),
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 3,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);

	// Registering your Custom Post Type
	register_post_type( 'coupons', $args );
}

add_filter( 'enter_title_here', 'coupons_change_title_text' );
function coupons_change_title_text( $title ) {
	$screen = get_current_screen();

	if ( 'coupons' == $screen->post_type ) {
		$title = esc_html__( 'Add Coupon Title', 'coupons' );
	}

	return $title;
}

add_action( 'init', 'coupons_brands_taxonomy', 0 );
function coupons_brands_taxonomy() {

	$labels = array(
		'name'                       => esc_html_x( 'Brands', 'taxonomy general name' ),
		'singular_name'              => esc_html_x( 'Brand', 'taxonomy singular name' ),
		'search_items'               => esc_html__( 'Search Brands' ),
		'popular_items'              => esc_html__( 'Popular Brands' ),
		'all_items'                  => esc_html__( 'All Brands' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => esc_html__( 'Edit Brand' ),
		'update_item'                => esc_html__( 'Update Brand' ),
		'add_new_item'               => esc_html__( 'Add New Brand' ),
		'new_item_name'              => esc_html__( 'New Brand Name' ),
		'separate_items_with_commas' => esc_html__( 'Separate Brands with commas' ),
		'add_or_remove_items'        => esc_html__( 'Add or remove Brands' ),
		'choose_from_most_used'      => esc_html__( 'Choose from the most used Brands' ),
		'menu_name'                  => esc_html__( 'Brands' ),
	);

	register_taxonomy( 'brands', 'coupons', array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'brand' ),
	) );
}

add_filter( 'manage_edit-brands_columns', 'coupons_brands_image_column' );
function coupons_brands_image_column( $columns ) {
	$brand_column['brand_image'] = 'Brand Image';

	return $columns + $brand_column;
}

add_filter( 'manage_brands_custom_column', 'coupons_brands_column_content', 10, 3 );
function coupons_brands_column_content( $content, $column_name, $term_id ) {
	switch ( $column_name ) {
		case 'brand_image':
			$data = get_term_meta( $term_id, 'brands_taxonomy_options', true );
			if ( $data ) {
				$image = $data['brand_logo'];
				echo wp_get_attachment_image( $image, 'image_100_100' );
			}
			break;
		default:
			break;
	}

	return $content;
}


class Rational_Meta_Box {
	private $screens = array(
		'coupons',
	);
	private $fields = array(
		array(
			'id' => 'exclusive',
			'label' => 'Exclusive',
			'type' => 'checkbox',
		),
		array(
			'id' => 'coupon-code',
			'label' => 'Coupon Code',
			'type' => 'text',
		),
		array(
			'id' => 'get-deal-url',
			'label' => 'Get Deal URL',
			'type' => 'text',
		),
		array(
			'id' => 'discount-percent',
			'label' => 'Discount Percent',
			'type' => 'text',
		),
		array(
			'id' => 'expiry-date',
			'label' => 'Expiry Date',
			'type' => 'date',
		),
	);

	/**
	 * Class construct method. Adds actions to their respective WordPress hooks.
	 */
	public function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
		add_action( 'save_post', array( $this, 'save_post' ) );
	}

	/**
	 * Hooks into WordPress' add_meta_boxes function.
	 * Goes through screens (post types) and adds the meta box.
	 */
	public function add_meta_boxes() {
		foreach ( $this->screens as $screen ) {
			add_meta_box(
				'add-coupon-code',
				__( 'Add Coupon Code', 'devo' ),
				array( $this, 'add_meta_box_callback' ),
				$screen,
				'advanced',
				'default'
			);
		}
	}

	/**
	 * Generates the HTML for the meta box
	 *
	 * @param object $post WordPress post object
	 */
	public function add_meta_box_callback( $post ) {
		wp_nonce_field( 'add_coupon_code_data', 'add_coupon_code_nonce' );
		$this->generate_fields( $post );
	}

	/**
	 * Generates the field's HTML for the meta box.
	 */
	public function generate_fields( $post ) {
		$output = '';
		foreach ( $this->fields as $field ) {
			$label = '<label for="' . $field['id'] . '">' . $field['label'] . '</label>';
			$db_value = get_post_meta( $post->ID, 'coupon_' . $field['id'], true );
			switch ( $field['type'] ) {
				case 'checkbox':
					$input = sprintf(
						'<input %s id="%s" name="%s" type="checkbox" value="1">',
						$db_value === '1' ? 'checked' : '',
						$field['id'],
						$field['id']
					);
					break;
				default:
					$input = sprintf(
						'<input %s id="%s" name="%s" type="%s" value="%s">',
						$field['type'] !== 'color' ? 'class="regular-text"' : '',
						$field['id'],
						$field['id'],
						$field['type'],
						$db_value
					);
			}
			$output .= $this->row_format( $label, $input );
		}
		echo '<table class="form-table"><tbody>' . $output . '</tbody></table>';
	}

	/**
	 * Generates the HTML for table rows.
	 */
	public function row_format( $label, $input ) {
		return sprintf(
			'<tr><th scope="row">%s</th><td>%s</td></tr>',
			$label,
			$input
		);
	}
	/**
	 * Hooks into WordPress' save_post function
	 */
	public function save_post( $post_id ) {
		if ( ! isset( $_POST['add_coupon_code_nonce'] ) )
			return $post_id;

		$nonce = $_POST['add_coupon_code_nonce'];
		if ( !wp_verify_nonce( $nonce, 'add_coupon_code_data' ) )
			return $post_id;

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return $post_id;

		foreach ( $this->fields as $field ) {
			if ( isset( $_POST[ $field['id'] ] ) ) {
				switch ( $field['type'] ) {
					case 'email':
						$_POST[ $field['id'] ] = sanitize_email( $_POST[ $field['id'] ] );
						break;
					case 'text':
						$_POST[ $field['id'] ] = sanitize_text_field( $_POST[ $field['id'] ] );
						break;
				}
				update_post_meta( $post_id, 'coupon_' . $field['id'], $_POST[ $field['id'] ] );
			} else if ( $field['type'] === 'checkbox' ) {
				update_post_meta( $post_id, 'coupon_' . $field['id'], '0' );
			}
		}
	}
}
new Rational_Meta_Box;