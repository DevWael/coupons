<?php
get_header();
?>
    <section class="no-posts-template text-center">
		<?php
		if ( is_rtl() ):
			?>
            <div class="devo_error">خطا ٤٠٤!</div>
            <div class="devo_text_error">
                عذراً الصفحة المطلوبة غير موجودة
            </div>
		<?php
		else:
			?>
            <div class="devo_error">404!</div>
            <div class="devo_text_error">
                Sorry! the page you are trying to reach is not available, it may be moved or deleted.
            </div>
		<?php
		endif;
		?>
    </section>

<?php
get_footer();
