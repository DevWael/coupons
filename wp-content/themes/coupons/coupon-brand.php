<?php
$terms            = $affiliate_url = '';
$exclusive        = get_post_meta( get_the_ID(), 'coupon_exclusive', true );
$coupon_code      = get_post_meta( get_the_ID(), 'coupon_coupon-code', true );
$get_deal_url     = get_post_meta( get_the_ID(), 'coupon_get-deal-url', true );
$discount_percent = get_post_meta( get_the_ID(), 'coupon_discount-percent', true );
$expiry_date      = get_post_meta( get_the_ID(), 'coupon_expiry-date', true );
$terms            = get_the_terms( get_the_ID(), 'brands' );
if ( ! empty( $terms ) ) {
	$affiliate_url = ( ! empty( get_term_meta( $terms[0]->term_id, 'brands_taxonomy_options', true ) ) && isset( get_term_meta( $terms[0]->term_id, 'brands_taxonomy_options', true )['affiliate_url'] ) ) ?
		get_term_meta( $terms[0]->term_id, 'brands_taxonomy_options', true )['affiliate_url'] : '';
}
?>
<div class="col-md-12">
    <div class="row item brand-box">
        <div class="col-2 top">
			<?php
			if ( ! empty( $discount_percent ) ):
				?>
                <span class="rate">
                    %<?php echo esc_html( $discount_percent ); ?>
                </span>
			<?php
			endif;
			?>
            <span class="user">
                <i class="fas fa-user"></i> <?php
				echo coupons_getPostViews();
				?>
            </span>
			
        </div>
        <div class="col-6  middle">
            <?php if ( $exclusive ) { ?>
                <span class="exclusive " title="<?php esc_attr_e( 'Exclusive', 'devolum' ); ?>" data-toggle="tooltip"
                      data-placement="top"><i class="fas fa-star"></i></span>
			<?php } ?>
			<?php
			the_title( '<h2>', '</h2>' );
			the_excerpt('<p>' , '</p>'  );
			?>
        </div>
        <div class="col-4  bottom">
			<?php
			if ( ! empty( $get_deal_url ) ):
				?>
                <a <?php if ( ! empty( $affiliate_url ) ){ ?>href="<?php echo esc_url( $affiliate_url ); ?>" <?php } ?>
                   onclick="window.open('<?php echo esc_url( get_permalink() ); ?>')"
                   title="<?php the_title_attribute() ?>">
                    <span class="coupon gradient"><?php esc_html_e( 'Get Deal', 'devolum' ); ?></span>
                </a>
			<?php
			else:
				?>
                <a <?php if ( ! empty( $affiliate_url ) ){ ?>href="<?php echo esc_url( $affiliate_url ); ?>" <?php } ?>
                   onclick="window.open('<?php echo esc_url( get_permalink() ); ?>')"
                   title="<?php the_title_attribute() ?>">
                    <span class="coupon"><?php esc_html_e( 'Get Coupon', 'devolum' ); ?></span>
                </a>
			<?php
			endif;
			?>
			<?php
			if ( ! empty( $expiry_date ) ):
				?>
                <span class="expire">
                <?php esc_html_e( 'Expires on: ', 'devolum' );
                echo esc_html( $expiry_date );
                ?>
                </span>
			<?php
			endif;
			?>
        </div>
    </div>
</div>
<!-- end of item -->



