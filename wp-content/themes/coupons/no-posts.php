<section class="no-posts-template text-center">
    <h2><?php esc_html_e( 'Sorry There are no Posts', 'devolum' ); ?></h2>
</section>