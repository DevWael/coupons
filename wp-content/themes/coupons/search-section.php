<div class="brands-serach">
    <div class="col-md-12">
        <div class="text-intro">
			<?php
			if ( coupons_get_setting( 'hero_title' ) ):
				?>
                <h2>
					<?php
					echo coupons_get_setting( 'hero_title' );
					?>
                </h2>
			<?php
			endif;
			if ( coupons_get_setting( 'hero_desc' ) ):
				?>
                <p><?php echo coupons_get_setting( 'hero_desc' ) ?></p>
			<?php
			endif;
			?>
        </div>
        <div class="serach-box">
            <form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
                <label for="serach">
                    <input type="text" id="serach" name="s" placeholder="<?php esc_attr_e( 'Search for coupons & start saving', 'devolum' ); ?>">
                    <input type="hidden" name="post_type" value="coupons">
                </label>
            </form>
        </div>
    </div>
</div>
<!-- end of brands Search -->