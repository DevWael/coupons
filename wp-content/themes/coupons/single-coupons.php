<?php
get_header();
//get_template_part( 'search', 'section' );
if ( have_posts() ): the_post();
	$exclusive        = get_post_meta( get_the_ID(), 'coupon_exclusive', true );
	$coupon_code      = get_post_meta( get_the_ID(), 'coupon_coupon-code', true );
	$get_deal_url     = get_post_meta( get_the_ID(), 'coupon_get-deal-url', true );
	$discount_percent = get_post_meta( get_the_ID(), 'coupon_discount-percent', true );
	$expiry_date      = get_post_meta( get_the_ID(), 'coupon_expiry-date', true );
	$terms            = wp_get_post_terms( get_the_ID(), 'brands' );
	$term_ids         = array();
	$title            = strip_tags( get_the_title() );
	$encoded_title    = str_replace( ' ', '+', $title );
	$url              = get_the_permalink();
	?>
    <div class="single-coupon">
        <div class="container">
            <div class="row ">
                <div class="col-md-12 ">
                    <div class="single-content">
                        <div class="heading">
							<?php
							the_title( '<h1>', '</h1>' );
							?>
                            <p class="info">
                                <i class="fas fa-store"></i> <span>
                                    <?php if ( $terms ):
	                                    foreach ( $terms as $term ):
		                                    $term_ids[] = $term->term_id;
		                                    ?>
                                            <a href="<?php echo esc_url( get_term_link( $term->term_id ) ); ?>"
                                               title="<?php echo esc_attr( $term->name ); ?>">
                                                <?php
                                                echo $term->name;
                                                ?>
                                            </a>
	                                    <?php
	                                    endforeach;
                                    endif;
                                    ?>
                                </span><i class="fas fa-user"></i>
                                <span>Used by <?php echo coupons_getPostViews(); ?></span>
                                <i class="far fa-calendar-times"></i><span
                                        title="Expires On"><?php _e( 'Expires On ', 'devolum' );
									echo $expiry_date;
									?></span>
                            </p>
                        </div>
						<?php
						if ( ! empty( $coupon_code ) ):
							?>
                            <div class="content">
                                <p class="coupon" id="coupon_val">
                                    <span id="coupon_val"><?php echo $coupon_code; ?></span>
                                    <button onclick="copyToClipboard('#coupon_val')" id="tooltip" data-toggle="tooltip"
                                            data-placement="top" title="copy"><i class="fas fa-copy"></i></button>
                                </p>
                            </div>
						<?php
                        elseif ( ! empty( $get_deal_url ) ):
							?>
                            <div class="content">
                                <p class="coupon" id="coupon_val">
									<?php esc_html_e( 'No coupon code needed, just go shopping & save', 'devolum' ); ?>
                                    <a target="_blank" href="<?php echo esc_url( $get_deal_url ) ?>"><?php esc_html_e( 'Here', 'devolum' ) ?></a>
                                </p>
                            </div>
						<?php
						endif;
						echo '<div class="newsletter-custom-form">';
						echo "<h4>" . __( 'Subscribe to get latest coupons and deals', 'devolum' ) . "</h4>";
						echo do_shortcode( '[mailpoet_form id="1"]' );
						echo '</div>';
						?>
                        <div class="share">
                            <ul>
                                <li>
                                    <a class="facebook"
                                       href="https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_url( get_permalink() ); ?>"
                                       onclick="window.open(this.href, 'facebook-share','width=580,height=296');return false;">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="twitter"
                                       href="https://twitter.com/share?text=<?php echo esc_attr( $encoded_title ); ?>&amp;url=<?php echo esc_url( $url ); ?>"
                                       onclick="window.open(this.href, 'twitter-share', 'width=550,height=235');return false;">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
endif;
$coupon_args = array(
	'post_type'      => 'coupons',
	'posts_per_page' => 4,
	'tax_query'      => array(
		array(
			'taxonomy' => 'brands',
			'field'    => 'term_id',
			'terms'    => $term_ids,
		),
	),
);

$coupon_query = new WP_Query( $coupon_args );
if ( $coupon_query->have_posts() && ! empty( $term_ids ) ):
	?>
    <div class="items-loop">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading">
                        <h2>Similar Coupons</h2>
                    </div>
                </div>
				<?php
				while ( $coupon_query->have_posts() ): $coupon_query->the_post();
					get_template_part( 'coupon', 'box' );
				endwhile;
				?>
            </div>
        </div>
    </div>
<?php endif;
wp_reset_postdata();
get_footer(); ?>
