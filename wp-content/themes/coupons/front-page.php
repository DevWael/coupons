<?php
get_header();
get_template_part( 'search', 'section' );
$terms = get_terms( array(
	'taxonomy'   => 'brands',
	'hide_empty' => true,
) );
if ( $terms ):
	?>
    <div class="brands">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="items">
                        <ul>
							<?php
							if ( $terms ):
								foreach ( $terms as $term ):
									$term_data = get_term_meta( $term->term_id, 'brands_taxonomy_options', true );
									?>
                                    <li>
                                        <a href="<?php echo esc_url( get_term_link( $term->term_id ) ); ?>">
											<?php
											if ( isset( $term_data['brand_logo'] ) && $term_data['brand_logo'] ):
												?>
                                                <img src="<?php echo esc_url( wp_get_attachment_image_src( $term_data['brand_logo'], 'image_260_160' )[0] ); ?>"
                                                     alt="<?php echo esc_attr( $term->name ); ?>">
											<?php
											endif;
											?>
                                        </a>
                                    </li>
								<?php
								endforeach;
							endif;
							?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
endif;
$coupon_args = array(
	'post_type'      => 'coupons',
	'posts_per_page' => 8,
	'meta_query'     => array(
		'post_views_count' => array(
			'key'  => 'devolum_post_views',
			'type' => 'NUMERIC'
		)
	),
	'orderby'        => 'meta_value_num',
);

$coupon_query = new WP_Query( $coupon_args );
if ( $coupon_query->have_posts() ):
	?>
    <div class="items-loop">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading">
                        <h2><?php esc_html_e( 'Top Coupons', 'devolum' ); ?></h2>
                    </div>
                </div>
				<?php
				while ( $coupon_query->have_posts() ): $coupon_query->the_post();
					get_template_part( 'coupon', 'box' );
				endwhile;
				?>
            </div>
        </div>
    </div>
<?php
endif;
wp_reset_postdata();
unset( $coupon_args['meta_query'], $coupon_args['orderby'], $coupon_args['posts_per_page'] );
$coupon_args['posts_per_page'] = 4;
$coupon_query                  = new WP_Query( $coupon_args );
if ( $coupon_query->have_posts() ):
	?>
    <div class="items-loop">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading">
                        <h2><?php esc_html_e( 'Last Added Coupons', 'devolum' ); ?></h2>
                    </div>
                </div>
				<?php
				while ( $coupon_query->have_posts() ): $coupon_query->the_post();
					get_template_part( 'coupon', 'box' );
				endwhile;
				?>
            </div>
        </div>
    </div>
<?php
endif;
wp_reset_postdata(); ?>
    <div class="more_posts">
        <a href="<?php echo esc_url( get_post_type_archive_link('coupons') ); ?>"><?php esc_html_e( 'More Coupons...', 'devolum' ); ?></a>
    </div>
<?php
get_footer();
