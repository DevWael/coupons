<?php
$coupons_theme = wp_get_theme();
define( 'COUPONS_NAME', $coupons_theme->name );
define( 'COUPONS_DIR', get_template_directory() );
define( 'COUPONS_URI', get_template_directory_uri() );
define( 'COUPONS_CSS_URI', COUPONS_URI . '/assets/css/' );
define( 'COUPONS_JS_URI', COUPONS_URI . '/assets/js/' );
define( 'COUPONS_IMAGES_DIR', COUPONS_URI . '/assets/images/' );
define( 'COUPONS_CORE', COUPONS_DIR . '/core/' );
define( 'COUPONS_CORE_URI', COUPONS_URI . '/core/' );
require_once COUPONS_CORE . 'init.php'; //Theme Core functions