<?php get_header();
if ( have_posts() ): ?>
    <div class="items-loop archive-file">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading">
                        <h2><?php if ( is_search() ) {
								_e( 'You serach for: ', 'devolum' );
								the_search_query();
							} else {
								echo get_the_archive_title();
							} ?>
                        </h2>
                    </div>
                </div>
				<?php
				while ( have_posts() ): the_post();
					get_template_part( 'post', 'box' );
				endwhile;
				?>
            </div>
			<?php coupons_pagination(); ?>
        </div>
    </div>
<?php
else:

	get_template_part( 'no', 'posts' );
endif;
get_footer();