<?php
$terms            = $affiliate_url =  $image_url_brand ='';
$exclusive        = get_post_meta( get_the_ID(), 'coupon_exclusive', true );
$coupon_code      = get_post_meta( get_the_ID(), 'coupon_coupon-code', true );
$get_deal_url     = get_post_meta( get_the_ID(), 'coupon_get-deal-url', true );
$discount_percent = get_post_meta( get_the_ID(), 'coupon_discount-percent', true );
$expiry_date      = get_post_meta( get_the_ID(), 'coupon_expiry-date', true );
$terms            = get_the_terms( get_the_ID(), 'brands' );
if ( ! empty( $terms ) ) {
	$affiliate_url = ( ! empty( get_term_meta( $terms[0]->term_id, 'brands_taxonomy_options', true ) ) && isset( get_term_meta( $terms[0]->term_id, 'brands_taxonomy_options', true )['affiliate_url'] ) ) ?
		get_term_meta( $terms[0]->term_id, 'brands_taxonomy_options', true )['affiliate_url'] : '';
        $image_id = get_term_meta( $terms[0]->term_id, 'brands_taxonomy_options', true )['brand_logo'];
        $image_url_brand = wp_get_attachment_image_src( $image_id, 'image_260_160' )[0];
}
?>
<div class="<?php if ( is_tax( 'brands' ) ) {
	echo 'col-md-4';
} else {
	echo 'col-md-3';
} ?>">
    <div class="item">
        <div class="top">
			<?php
			if ( ! empty( $discount_percent ) ):
				?>
                <span class="rate">
                    %<?php echo esc_html( $discount_percent ); ?>
                </span>
			<?php
			endif;
			?>
            <span class="user">
                <i class="fas fa-user"></i> <?php
				echo coupons_getPostViews();
				?>
            </span>
			<?php if ( $exclusive ) { ?>
                <span class="exclusive " title="<?php esc_attr_e( 'Exclusive', 'devolum' ); ?>" data-toggle="tooltip"
                      data-placement="top"><i class="fas fa-star"></i></span>
			<?php } ?>
        </div>
        <div class="middle">
          
			<?php 
			if ( ! empty( $get_deal_url ) ):
				?>
                <a <?php if ( ! empty( $affiliate_url ) ){ ?>href="<?php echo esc_url( $affiliate_url ); ?>" <?php } ?>
                   onclick="window.open('<?php echo esc_url( get_permalink() ); ?>')"
                   title="<?php the_title_attribute() ?>">
					<?php if($image_url_brand): ?>
                  		<img src="<?php echo $image_url_brand; ?>" alt="<?php the_title_attribute() ?>">
                  	<?php else: ?>
						<?php the_post_thumbnail( 'image_260_160' ); ?>
                 	<?php endif; ?>
                </a>
			<?php
			else:
				?>
                <a <?php if ( ! empty( $affiliate_url ) ){ ?>href="<?php echo esc_url( $affiliate_url ); ?>" <?php } ?>
                   onclick="window.open('<?php echo esc_url( get_permalink() ); ?>')"
                   title="<?php the_title_attribute() ?>">
                  <?php if($image_url_brand): ?>
                  	<img src="<?php echo $image_url_brand; ?>" alt="<?php the_title_attribute() ?>">
                  <?php else: ?>
					<?php the_post_thumbnail( 'image_260_160' ); ?>
                  <?php endif; ?>
                </a>
			<?php
			endif;
			the_title( '<h2>', '</h2>' )
			?>
        </div>
        <div class="bottom">
			<?php
			if ( ! empty( $get_deal_url ) ):
				?>
                <a <?php if ( ! empty( $affiliate_url ) ){ ?>href="<?php echo esc_url( $affiliate_url ); ?>" <?php } ?>
                   onclick="window.open('<?php echo esc_url( get_permalink() ); ?>')"
                   title="<?php the_title_attribute() ?>">
                    <span class="coupon gradient"><?php esc_html_e( 'Get Deal', 'devolum' ); ?></span>
                </a>
			<?php
			else:
				?>
                <a <?php if ( ! empty( $affiliate_url ) ){ ?>href="<?php echo esc_url( $affiliate_url ); ?>" <?php } ?>
                   onclick="window.open('<?php echo esc_url( get_permalink() ); ?>')"
                   title="<?php the_title_attribute() ?>">
                    <span class="coupon"><?php esc_html_e( 'Get Coupon', 'devolum' ); ?></span>
                </a>
			<?php
			endif;
			?>
			<?php
			if ( ! empty( $expiry_date ) ):
				?>
                <span class="expire">
                <?php esc_html_e( 'Expires on: ', 'devolum' );
                echo esc_html( $expiry_date );
                ?>
                </span>
			<?php
			endif;
			?>
        </div>
    </div>
</div>
<!-- end of item -->



