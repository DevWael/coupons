<div class="main-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="copyrights">
                    <p>© 2018 Couponramy.com All copyrights reserved - By
                        <a href="https://devolum.com" target="_blank" title="Design & Develop by Devolum labs">Devolum
                            Labs</a>
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="social-icons">
                    <ul>
						<?php if ( coupons_get_setting( 'facebook' ) ): ?>
                            <li>
                                <a href="<?php echo coupons_get_setting( 'facebook' ); ?>" target="_blank"
                                   title="facebook page"><i class="fab fa-facebook"></i></a>
                            </li>
						<?php
						endif;
						?>
						<?php if ( coupons_get_setting( 'twitter' ) ): ?>
                            <li>
                                <a href="<?php echo coupons_get_setting( 'twitter' ); ?>" target="_blank"
                                   title="twitter account"><i
                                            class="fab fa-twitter-square"></i></a>
                            </li>
						<?php
						endif;
						?>
						<?php if ( coupons_get_setting( 'instagram' ) ): ?>
                            <li>
                                <a href="<?php echo coupons_get_setting( 'instagram' ); ?>" target="_blank"
                                   title="instagram account"><i class="fab fa-instagram"></i></a>
                            </li>
						<?php
						endif;
						?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php wp_footer();
if ( is_singular() ):
	?>
    <script>
        function copyToClipboard(element) {
            var $temp = jQuery("<input>");
            jQuery("body").append($temp);
            $temp.val(jQuery(element).text()).select();
            document.execCommand("copy");
            setTimeout(function () {
                jQuery('#tooltip').tooltip('show').attr('data-original-title', 'Copied').tooltip('show');
            }, 0);
            $temp.remove();
            setTimeout(function () {
                jQuery('#tooltip').tooltip('show').attr('data-original-title', 'Copy').tooltip('hide');
            }, 1000);
        }
    </script>
<?php
endif;
?>
</body>
</html>