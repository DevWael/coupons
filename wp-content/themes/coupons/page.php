<?php
get_header();
if ( have_posts() ): the_post();
	$title         = strip_tags( get_the_title() );
	$encoded_title = str_replace( ' ', '+', $title );
	$url           = get_the_permalink();
	?>
	<div class="single-coupon single_only">
		<div class="container">
			<div class="row ">
				<div class="col-md-12 ">
					<div class="single-content">
						<div class="heading">
							<?php
							the_title( '<h1>', '</h1>' );
							?>
						</div>
							<div class="content">
								<?php the_content(); ?>
							</div>
						<div class="share">
							<ul>
								<li>
									<a class="facebook"
									   href="https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_url( get_permalink() ); ?>"
									   onclick="window.open(this.href, 'facebook-share','width=580,height=296');return false;">
										<i class="fab fa-facebook-f"></i>
									</a>
								</li>
								<li>
									<a class="twitter"
									   href="https://twitter.com/share?text=<?php echo esc_attr( $encoded_title ); ?>&amp;url=<?php echo esc_url( $url ); ?>"
									   onclick="window.open(this.href, 'twitter-share', 'width=550,height=235');return false;">
										<i class="fab fa-twitter"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
endif;
?>


<?php

get_footer(); ?>
