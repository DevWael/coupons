<div class="col-md-4">
    <div class="post_box" itemscope itemtype="http://schema.org/Article" role="article">
        <a href="<?php echo esc_url( get_permalink() ); ?>"
           title="<?php the_title_attribute() ?>">
			<?php the_post_thumbnail( 'image_350_220' ); ?>
        </a>

        <a href="<?php echo esc_url( get_permalink() ); ?>"
           title="<?php the_title_attribute() ?>">
			<?php
			the_title( '<h2>', '</h2>' )
			?>
        </a>
        <div class="meta">
            <span class="category" title="<?php esc_attr_e( 'Category', 'window-mag' ); ?>">
				<i class="fas fa-folder-open"></i>
	            <?php the_category( ' ', '' ); ?>
			</span>
            <span class="post-date" title="<?php esc_attr_e( 'Created on', 'window-mag' ); ?>">
			<i class="far fa-calendar-alt"></i>
			<meta itemprop="datePublished" content="<?php the_date( 'Y-m-d' ) ?>">
		        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php echo get_the_time( get_option( 'date_format' ) ); ?>
                </a>
		</span>
        </div>
    </div>
</div>
<!-- end of item -->



