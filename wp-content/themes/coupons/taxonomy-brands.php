<?php get_header();
if ( have_posts() ):
	$taxonomy = get_queried_object();
	$image_id = get_term_meta( $taxonomy->term_id, 'brands_taxonomy_options', true )['brand_logo'];
	$image_url_brand = wp_get_attachment_image_src( $image_id, 'image_260_160' )[0];
	?>
    <div class="items-loop archive-file">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="brand-page">
                        <div class="brand-image">
                            <img src="<?php echo $image_url_brand ?>" alt="<?php echo $taxonomy->name; ?>">
                        </div>
                        <div class="brand_title">
                            <h1><?php echo $taxonomy->name; ?></h1>
                        </div>
                        <div class="brand_description">
                            <p>
								<?php echo $taxonomy->description; ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="row">

                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home"
                                   role="tab" aria-controls="nav-home"
                                   aria-selected="true"><?php _e( 'Coupons', 'devolum' ); ?></a>
                                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile"
                                   role="tab" aria-controls="nav-profile"
                                   aria-selected="false"><?php _e( 'Deals', 'devolum' ); ?></a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                 aria-labelledby="nav-home-tab">

								<?php
								while ( have_posts() ): the_post();
									$coupon_code  = get_post_meta( get_the_ID(), 'coupon_coupon-code', true );
									if ( ! empty( $coupon_code ) ) {
										get_template_part( 'coupon', 'brand' );
									}

								endwhile; wp_reset_postdata();
								?>
                            </div>
                            <div class="tab-pane fade" id="nav-profile" role="tabpanel"
                                 aria-labelledby="nav-profile-tab">
								<?php
								while ( have_posts() ): the_post();
									$get_deal_url = get_post_meta( get_the_ID(), 'coupon_get-deal-url', true );
									if ( ! empty( $get_deal_url ) ) {
										get_template_part( 'coupon', 'brand' );
									}

								endwhile;
								?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?php coupons_pagination(); ?>
        </div>
    </div>
<?php
else:
	get_template_part( 'no', 'posts' );
endif;
get_footer();